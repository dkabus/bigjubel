#ifndef BIGJUBEL_H
#define BIGJUBEL_H

#include "language.h"
#include "functions.h"
#include "jubel.h"

enum Output { Simple, Colored, Yaml };

class BigJubel
{
private:
  const int m_startday;
  const int m_stopday;
  const int m_maxjubels;
  const enum Output m_output;
  const Language& lang;

public:
  BigJubel(int start=1, int stop=1, int max=-1, enum Output output=Output::Simple, const Language& lang=Language("de"));
  void run();
  Jubels doTheJubel(const Events& events) const;
  Events readEvents() const;
};

#endif
