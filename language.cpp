#include "language.h"

const Dictionary de = {
  {"headline1", Word{"Jubiläen vom"}},
  {"headline2", Word{"bis zum"}},
  {"year", Word{
    {"Jahr", "Jahres", "Jahr", "Jahr"},
    {"Jahre", "Jahre", "Jahren", "Jahre"},
  }},
  {"month", Word{
    {"Monat", "Monats", "Monat", "Monat"},
    {"Monate", "Monate", "Monaten", "Monate"},
  }},
  {"week", Word{
    "Woche", // same in all cases
    "Wochen",
  }},
  {"day", Word{
    {"Tag", "Tages", "Tag", "Tag"},
    {"Tage", "Tage", "Tagen", "Tage"},
  }},
};

const Dictionary en = {
  {"headline1", Word{"Jubilees from"}},
  {"headline2", Word{"through"}},
  {"year", Word{"year", "years"}},
  {"month", Word{"month", "months"}},
  {"week", Word{"week", "weeks"}},
  {"day", Word{"day", "days"}},
};

const std::map<str, const Dictionary&> languages = {
  {"C", en}, // fallback
  {"de", de}, // german
  {"en", en}, // english
};

Word::Word(const str& word)
: Word(word, word)
{ }

Word::Word(const str& singular, const str& plural)
: Word(strs{singular}, strs{plural})
{ }

Word::Word(const strs& singulars, const strs& plurals)
: singulars(singulars), plurals(plurals)
{
  if(singulars.size() != plurals.size())
    throw "Invalid definition of word '" + operator()() + "'";
}

str Word::operator()(int number, size_t gcase) const
{
  return operator()(abs(number) != 1, gcase);
}

str Word::operator()(bool plural, size_t gcase) const
{
  gcase = gcase % n_cases();
  return plural ? plurals[gcase] : singulars[gcase];
}

Language::Language(const str& language_code)
try : code(language_code), dict(languages.at(language_code))
{ }
catch( std::out_of_range error )
{
  str err = str("Invalid language code: ") + language_code + ". Available codes:";
  for (auto it = languages.begin(); it != languages.end(); ++it)
  { err += str(" ") + it->first; }
  throw err;
}

const Word& Language::operator()(const str& key) const
{
  try {
    return dict.at(key);
  }
  catch( std::out_of_range )
  {
    throw str("Key not in dictionary for language '") + code + str("': ") + key;
  }
}
