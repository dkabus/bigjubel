#ifndef JUBEL_H
#define JUBEL_H

#include "event.h"
#include "language.h"
using Unit = Word;

class Jubel {
  public:
    Jubel(const Event&, const Date&, const Unit&, int, bool tf=0, bool yf=0);
    str to_str(bool colored=false) const;
    str to_yaml() const;
  private:
    Event m_event;
    Date m_date;
    Unit m_unit;
    int m_number;
    bool m_todayflag;
    bool m_yearflag;
};

using Jubels = std::vector<Jubel>;

#endif
