#ifndef LANGUAGE_H
#define LANGUAGE_H

#include "includes.h"

class Word {
  public:
    Word(const str& word);
    Word(const str& singular, const str& plural);
    Word(const strs& singulars, const strs& plurals);
    str operator()(int number, size_t gcase=0) const;
    str operator()(bool plural=false, size_t gcase=0) const;
    size_t n_cases() const { return singulars.size(); }
  private:
    const strs singulars;
    const strs plurals;
};

using Dictionary = std::map<str, Word>;

class Language {
  public:
    Language(const str& language_code);
    const Word& operator()(const str& key) const;
    const str code;
  private:
    const Dictionary& dict;
};

#endif
