#include "date.h"

static const struct tm constructTm(int year, int month, int day)
{
  struct tm t = {0};
  t.tm_sec = t.tm_min = t.tm_hour = 0; // midnight
  t.tm_mon = month - 1;
  t.tm_year = year - 1900;
  t.tm_mday = day;
  t.tm_isdst = -1; // unknown
  return t;
}

Date::Date(int offset)
{
  m_time = std::time(NULL);
  addDay(offset);
}

Date::Date(const struct tm& date)
{
  set(date);
}

Date::Date(int year, int month, int day)
: Date(constructTm(year, month, day))
{ }

Date::Date(const str& year, const str& month, const str& day)
: Date(std::stoi(year), std::stoi(month), std::stoi(day))
{ }

struct tm Date::get() const
{
  return *std::localtime(&m_time);
}

void Date::set(const struct tm& date)
{
  struct tm date_ = date;
  m_time = std::mktime(&date_);
}

void Date::addDay(int offset)
{
  struct tm date = get();
  date.tm_mday += offset;
  date.tm_isdst = -1;
  set(date);
}

str Date::to_str() const
{
  const struct tm date = get();
  char cstr[30];
  // std::strftime(cstr, 30, "%F", &date);
  std::strftime(cstr, 30, "%d.%m.%Y", &date);
  return str(cstr);
}
