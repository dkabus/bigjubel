#ifndef DATE_H
#define DATE_H

#include "includes.h"

class Date {
  using Time = std::time_t;
  public:
    Date(int offset=0);
    Date(const struct tm& date);
    Date(int, int, int);
    Date(const str&, const str&, const str&);
    struct tm get() const;
    void set(const struct tm& date);
    void addDay(int offset);
    str to_str() const;
  private:
    Time m_time;
};

#endif
