#include "bigjubel.h"
#include <locale.h>

str version = "10.2.3";
str copyright = "BigJubel " + version + "\n\n"
	"© 1999 - 2020 DAKsoft\n"
	"© 1999 - 2020 Diethelm Kabus\n"
	"© 2019 - 2020 Desmond Kabus\n";
str license_notice =
    "This program is free software: you can redistribute it and/or modify\n"
    "it under the terms of the GNU General Public License as published by\n"
    "the Free Software Foundation, either version 3 of the License, or\n"
    "(at your option) any later version.\n"
	"\n"
    "This program is distributed in the hope that it will be useful,\n"
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
    "GNU General Public License for more details.\n";

int main(int argc, char *argv[])
{
  setlocale(LC_ALL, "");
  str lang_code = str{setlocale(LC_CTYPE, NULL)}.substr(0, 2);

  int opt;
  int startday = 100;
  int stopday = 266;
  int maxjubels = -1;
  enum Output output = Output::Simple;
  bool show_help = false;
  str infile = isatty(fileno(stdin)) ? "BigJubel.txt" : "-";
  str outfile = "-";
  while ((opt = getopt(argc, argv, "b:e:l:m:i:o:cyhv")) != -1) {
    switch (opt) {
    case 'b':
      startday = atoi(optarg);
      break;
    case 'e':
      stopday = atoi(optarg);
      break;
    case 'l':
      lang_code = str(optarg);
      break;
    case 'm':
      maxjubels = atoi(optarg);
      break;
    case 'i':
      infile = str(optarg);
      break;
    case 'o':
      outfile = str(optarg);
      break;
    case 'c':
      output = Output::Colored;
      break;
    case 'y':
      output = Output::Yaml;
      break;
    case 'v':
      std::cout << version << std::endl;
      return EXIT_SUCCESS;
    case 'h':
      show_help = true;
    default:
      std::cerr << copyright << std::endl;
	  std::cerr << license_notice << std::endl;
      std::cerr << "Usage: " << argv[0] << " [OPTIONS]" << std::endl;
      std::cerr << std::endl;
      std::cerr << "Options:" << std::endl;
      std::cerr << "  -h         ";
      std::cerr << "Print this help text and exit" << std::endl;
      std::cerr << "  -b NUMBER  ";
      std::cerr << "Number of days to check for jubilees before today" << std::endl;
      std::cerr << "  -e NUMBER  ";
      std::cerr << "Number of days to check for jubilees after today" << std::endl;
      std::cerr << "  -m NUMBER  ";
      std::cerr << "Maximum number of jubels to output" << std::endl;
      std::cerr << "  -l LANGUAGE";
      std::cerr << "Two letter language code of the desired language" << std::endl;
      std::cerr << "  -i FILE    ";
      std::cerr << "Filename of input (default is 'BigJubel.txt', '-' for standard input)" << std::endl;
      std::cerr << "  -o FILE    ";
      std::cerr << "Filename for output (default is '-', i.e. standard output)" << std::endl;
      std::cerr << "  -v         ";
      std::cerr << "Print version and exit" << std::endl;
      std::cerr << "  -c         ";
      std::cerr << "Use colored formatting for output using VT100 escape sequences" << std::endl;
      std::cerr << "  -y         ";
      std::cerr << "Use YAML formatting for output" << std::endl;
      return show_help ? EXIT_SUCCESS : EXIT_FAILURE;
    }
  }

  // redirect cin to input file and cout to output file
  std::ifstream in;
  std::ofstream out;
  std::streambuf* cinbuf = std::cin.rdbuf();
  std::streambuf* coutbuf = std::cout.rdbuf();
  if(infile != "-")
  {
    in = std::ifstream(infile.c_str());
    std::cin.rdbuf(in.rdbuf());
  }
  if(outfile != "-")
  {
    out = std::ofstream(outfile.c_str());
    std::cout.rdbuf(out.rdbuf());
  }

  try {
    Language lang(lang_code);
    BigJubel bigjubel(startday, stopday, maxjubels, output, lang);
    bigjubel.run();
  }
  catch(str error)
  {
    std::cerr << "Fatal error: " << error << std::endl;
    return EXIT_FAILURE;
  }

  // switch back to cin and cout
  std::cin.rdbuf(cinbuf);
  std::cout.rdbuf(coutbuf);
  return EXIT_SUCCESS;
}
