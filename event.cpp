#include "event.h"

static inline bool inRange(int a, int b, int c)
{ return a <= b and b <= c; }

static inline void assert(bool b, str error)
{ if(not b) throw error; }

Event::Event(const str& line)
{
  int pos = line.find("\r");
  str line_ = line;
  if (pos != -1)
    line_.replace(pos, pos+1, "");
  std::istringstream s(line_);

  str value;
  std::vector<str> values;
  for(int i=0; i<3; i++)
  {
    if(std::getline(s, value, '.'))
      values.push_back(value);
    else
      throw str("Ungültiges Datum:\n" + line);
  }

  if(std::getline(s, value))
    values.push_back(value);
  else
    throw str("Keine Beschreibung gefunden:\n" + line);

  assert(inRange(1, values[0].length(), 2), "Ungültiger Tag:\n" + line);
  assert(inRange(1, values[1].length(), 2), "Ungültiger Monat:\n" + line);
  assert(inRange(1, values[2].length(), 4), "Ungültiges Jahr:\n" + line);
  assert(3 <= values[3].length(), "Ungültige Beschreibung:\n" + line);

  m_date = Date(values[2], values[1], values[0]);

  if( ( pos = values[3].find("#") ) != str::npos )
  {
    m_text0 = values[3].substr(0,pos);
    m_text1 = values[3].substr(pos+1);
    m_case = 0;
  }
  else if( ( pos = values[3].find("{") ) != str::npos )
  {
    m_text0 = values[3].substr(0,pos);
    m_text1 = values[3].substr(pos+1);
    pos = m_text1.find("}");
    assert(pos != str::npos, "Keine Klammer } gefunden:\n" + line);
    m_case = std::stoi(m_text1.substr(0, pos));
    m_text1 = m_text1.substr(pos+1);
  }
  else
  {
    throw str("Keinen Marker # oder {case} in Beschreibung gefunden:\n" + line);
  }
}

Event::Event(const Date& date, const str& text0, const str& text1, const size_t gcase)
{
    m_date = date;
    m_text0 = text0;
    m_text1 = text1;
    m_case = gcase;
}

str Event::to_str(const str& numberAndLabel) const
{
    str s = m_text0 + numberAndLabel + m_text1;
    return s;
}

str Event::to_str(const str& number, const str& label) const
{
    return to_str(number + " " + label);
}

str Event::to_str(int number, const str& label) const
{
    return to_str(std::to_string(number), label);
}

str Event::to_str(int number, const Unit& unit) const
{
    return to_str(std::to_string(number), unit(number, m_case));
}

str Event::to_str() const
{
    return m_date.to_str() + "." + to_str("{" + std::to_string(m_case) + "}");
}
