#include "bigjubel.h"

BigJubel::BigJubel(int start, int stop, int max, enum Output output, const Language& lang)
: m_startday(start), m_stopday(stop), m_maxjubels(max), m_output(output), lang(lang)
{}

void BigJubel::run()
{
  Date heute;
  Date start(-m_startday);
  Date stop(m_stopday);

  Events events = readEvents();
  Jubels jubels = doTheJubel(events);

  str hlbuffer = lang("headline1")() + " " + start.to_str() + " "
  + lang("headline2")() + " " + stop.to_str();

  if(m_output == Output::Yaml)
  {
    std::cout << "title: " << hlbuffer << std::endl;
    std::cout << "jubels: " << std::endl;
    for(Jubel jubel : jubels)
      std::cout << "-\n" << jubel.to_yaml();
  }
  else // if(m_output == Output::Simple || m_output == Output::Colored)
  {
	if(m_output == Output::Colored)
	{ hlbuffer = str("\033[92;4;1m") + hlbuffer + "\033[m"; }
    std::cout << hlbuffer << std::endl;
    for(Jubel jubel : jubels)
      std::cout << jubel.to_str(m_output) << std::endl;
  }
}

Events BigJubel::readEvents() const
{
  str line;
  Events events;
  while(std::getline(std::cin, line)) {
    if( not line.empty() and char(line[0]) != '#' )
      events.push_back(Event(line));
  }
  return events;
}

Jubels BigJubel::doTheJubel(const Events& events) const
{
  Jubels jubels;
  Date heute;
  int dif;
  struct tm tmjubltag, tmheute;
  str line;

  for (int i = -m_startday; i <= m_stopday; i++)
  {
    heute = Date(i);

    tmheute = heute.get();
    for(Event event : events)
    {
      // Texte und Datum extrahieren
      tmjubltag = event.getDate().get();

      if( m_maxjubels >= 0 and jubels.size() >= m_maxjubels )
        return jubels;
      else if((dif = YearTest(tmheute,tmjubltag)) and dif > 0)
        jubels.push_back(Jubel(event, heute, lang("year"), dif, i==0, true));
      else if((dif = MonTest(tmheute,tmjubltag)) and dif > 0)
        jubels.push_back(Jubel(event, heute, lang("month"), dif, i==0));
      else if((dif = WeekTest(tmheute,tmjubltag)) and dif > 0)
        jubels.push_back(Jubel(event, heute, lang("week"), dif, i==0));
      else if((dif = DayTest(tmheute,tmjubltag)) and dif > 0)
        jubels.push_back(Jubel(event, heute, lang("day"), dif, i==0));
    }
  }
  return jubels;
}
