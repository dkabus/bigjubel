#include "functions.h"

/***********************************************************************
* #DAK# YearTest                                                       *
*       testet auf Jubilaeumsjahr und gibt 0 bzw die Anzahl der Jahre  *
*       zurueck.                                                       *
***********************************************************************/

int YearTest(const tm& heute, const tm& jubltag)
{
  int  dif;

  if ( heute.tm_mday == jubltag.tm_mday &&/* auf ganze Jahre testen */
       heute.tm_mon  == jubltag.tm_mon)
  {
    dif=(heute.tm_year-jubltag.tm_year);
    return dif;
  }
  else
    return 0;
}
/***********************************************************************
* #DAK# MonTest                                                        *
*       testet auf Jubilaeumsmonat und gibt 0 bzw die Anzahl der Mona- *
*       te zurueck.                                                    *
***********************************************************************/

int MonTest(const tm& heute, const tm& jubltag)
{
  int  dif;

  if (heute.tm_mday == jubltag.tm_mday)    // auf Jubelmonate testen
  {
    dif=mondif(jubltag,heute);
    if(isjubel(dif))
      return dif;
    else
      return 0;
  }
  else
    return 0;
}
/***********************************************************************
* #DAK# WeekTest                                                       *
*       testet auf Jubilaeumswoche und gibt 0 bzw die Anzahl der Wo-   *
*       chen zurueck.                                                  *
***********************************************************************/

int WeekTest(const tm& heute, const tm& jubltag)
{
  int  dif;

  dif=tagdif(jubltag,heute);
  if ((dif % 7) == 0)                       /* auf Jubelwochen testen */
  {
    dif=dif/7;
    if(isjubel(dif))
      return dif;
    else
      return 0;
  }
  else
    return 0;
}
/***********************************************************************
* #DAK# DayTest                                                        *
*       testet auf Jubilaeumstag und gibt 0 bzw die Anzahl der Tage    *
*       zurueck.                                                       *
***********************************************************************/

int DayTest(const tm& heute, const tm& jubltag)
{
  int  dif;

  dif=tagdif(jubltag,heute);
  if(isjubel(dif))
    return dif;
  else
    return 0;
  return dif;
}

int mondif(const tm& tag1, const tm& tag2)     // gibt die Monatsdifferenz
{
  int dif=0;

  if(tag2.tm_year - tag1.tm_year > 1)          // if ganze Jahre dazwischen
    dif=dif+12*(tag2.tm_year - tag1.tm_year - 1);
  if(tag2.tm_year - tag1.tm_year >= 1)         // Monate der angebrochenen
    dif=dif+(12-tag1.tm_mon)+tag2.tm_mon;      // Jahre addieren
  if(tag2.tm_year - tag1.tm_year == 0)         // falls im selben Jahr
    dif=tag2.tm_mon-tag1.tm_mon;
  if(tag2.tm_year - tag1.tm_year < -1)         // if ganze Jahre dazwischen
    dif=dif+12*(tag2.tm_year - tag1.tm_year + 1);
  if(tag2.tm_year - tag1.tm_year <= -1)        // Monate der angebrochenen
    dif=dif-(12-tag2.tm_mon)-tag1.tm_mon;      // Jahre addieren
  return dif;
}

unsigned char isjubel(int zahl)         // gucken ob es eine Jubelzahl ist
{
  if ( abs(zahl) <=  12 ) return 1;
  if (snzahl(zahl)) return 1;

  if ((zahl%   5==0) && (abs(zahl)<   100)) return 1;
  if ((zahl%  10==0) && (abs(zahl)<   200)) return 1;
  if ((zahl%  25==0) && (abs(zahl)<   500)) return 1;
  if ((zahl%  50==0) && (abs(zahl)<  1000)) return 1;
  if ((zahl% 100==0) && (abs(zahl)<  2000)) return 1;
  if ((zahl% 250==0) && (abs(zahl)<  5000)) return 1;
  if ((zahl% 500==0) && (abs(zahl)< 10000)) return 1;
  if ((zahl%1000==0) && (abs(zahl)< 20000)) return 1;
  if (zahl%2500==0) return 1;
  if (zahl%5000==0) return 1;

  return 0;
}

int tagdif(const tm& tag1, const tm& tag2)     // gibt die Tagesdifferenz
{                                                // von 2 Tagesdaten zurueck
  int i, dif=0;

  if(tag2.tm_year - tag1.tm_year > 1)          // if ganze Jahre dazwischen
  {
    for(i = tag1.tm_year+1;i < tag2.tm_year; i++)
    {
      dif=dif+365;                               // Tage dazuaddieren
      if(i%4 == 0) dif++;                        // und Schaltjahre beachten
    }
  }
  if(tag2.tm_year - tag1.tm_year >= 1)
  {
    if(tag1.tm_year % 4 == 0)                   // Tage der angebrochenen
      dif=dif+(366-tag1.tm_yday)+tag2.tm_yday; // Jahre zusammenzaehlen
    else                                         // unter Beruecksichtigung
      dif=dif+(365-tag1.tm_yday)+tag2.tm_yday; // der Schaltjahre
  }
  if(tag2.tm_year - tag1.tm_year == 0)         // wenn beide Tage im selben
    dif = tag2.tm_yday - tag1.tm_yday;         // Jahr . Differenz bilden
  if(tag2.tm_year - tag1.tm_year < -1)         // if ganze Jahre dazwischen
  {
    for(i = tag1.tm_year+1;i > tag2.tm_year; i--)
    {
      dif=dif-365;                               // Tage dazuaddieren
      if(i%4 == 0) dif--;                        // und Schaltjahre beachten
    }
  }
  if(tag2.tm_year - tag1.tm_year <= -1)
  {
    if(tag2.tm_year % 4 == 0)                   // Tage der angebrochenen
      dif=dif-(366-tag2.tm_yday)-tag1.tm_yday; // Jahre zusammenzaehlen
    else                                         // unter Beruecksichtigung
      dif=dif-(365-tag2.tm_yday)-tag1.tm_yday; // der Schaltjahre
  }
  return dif;
}

unsigned char snzahl(int zahl)          // gucken ob es eine Schnapszahl ist
{
  char buffer[8];
  unsigned char schnaps=1, i;

  zahl=abs(zahl);
  sprintf(buffer,"%d",zahl); // in Zeichenkette umwandeln

  for(i=1;i<strlen(buffer);i++)         // auf Schnapszahl ueberpruefen
    if(buffer[i]!=buffer[0])schnaps=0;

  if(schnaps)return 1;
  else       return 0;
}
