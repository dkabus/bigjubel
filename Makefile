headers = $(shell ls *.h)
sources = $(shell ls *.cpp)
objects = $(sources:.cpp=.o)
CC=g++

all: bigjubel

bigjubel: $(objects)
	$(CC) -o $@ $^

$(objects): %.o: %.cpp
	$(CC) -c $<

clean:
	rm -f bigjubel $(objects)

install: all
	install -Dm755 "bigjubel" -t "$(DESTDIR)/usr/bin/"
	install -Dm644 "LICENSE" -t "$(DESTDIR)/usr/share/licenses/bigjubel/"
	mkdir -p "$(DESTDIR)/usr/share/man/man1"
	sed "s/VERSION/$(shell ./bigjubel -v)/g" < bigjubel.1 \
		> "$(DESTDIR)/usr/share/man/man1/bigjubel.1"

uninstall:
	rm -f "$(DESTDIR)/usr/bin/bigjubel"
	rm -f "$(DESTDIR)/usr/share/licenses/bigjubel/LICENSE"
	rmdir "$(DESTDIR)/usr/share/licenses/bigjubel"
	rm -f "$(DESTDIR)/usr/share/man/man1/bigjubel.1"

.PHONY: all clean install uninstall
