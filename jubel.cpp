#include "jubel.h"

Jubel::Jubel(const Event& event, const Date& date, const Unit& unit, int n, bool tf, bool yf)
: m_event(event), m_date(date), m_unit(unit), m_number(n), m_todayflag(tf), m_yearflag(yf)
{ }

str Jubel::to_str(bool colored) const
{
  if(colored)
  {
	str y = str(m_yearflag ? "\033[93;1m" : "");
	str t = str(m_todayflag ? "\033[7m" : "");
	str r = str(m_yearflag || m_todayflag ? "\033[m" : "");
    return y+t + m_date.to_str() +r+"\t"+y+ m_event.to_str(m_number, m_unit) + r;
  }
  else
  {
    return m_date.to_str()
    + " "  + (m_todayflag ? "~" : " ")
    + " "  + (m_yearflag  ? "*" : " ")
    + "\t" + m_event.to_str(m_number, m_unit);
  }
}

static inline str yaml_pair(const str& key, const str& value)
{
  return "  " + key + ": " + value + "\n";
}

static inline str yaml_pair(const str& key, int value)
{
  return "  " + key + ": " + std::to_string(value) + "\n";
}

str Jubel::to_yaml() const
{
  str s = yaml_pair("date", m_date.to_str());
  s += yaml_pair("message", m_event.to_str(m_number, m_unit));
  s += yaml_pair("todayflag", m_todayflag);
  s += yaml_pair("yearflag",  m_yearflag);
  return s;
}
