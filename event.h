#ifndef EVENT_H
#define EVENT_H

#include "date.h"
#include "language.h"
using Unit = Word;

class Event {
  public:
    Event(const str& line);
    Event(const Date& date, const str& text0, const str& text1, size_t gcase=0);
    const Date& getDate() const { return m_date; }
    str to_str(const str& numberAndLabel) const;
    str to_str(const str& number, const str& label) const;
    str to_str(int number, const str& label) const;
    str to_str(int number, const Unit& unit) const;
    str to_str() const;
  private:
    Date m_date;
    str m_text0; // the part of the text before the timespan
    str m_text1; // and after the timespan
    size_t m_case; // the grammatical case to use for added timespan
};

using Events = std::vector<Event>;

#endif
