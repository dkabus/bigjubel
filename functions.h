#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "date.h"

int YearTest(const tm&, const tm&);
int MonTest(const tm&, const tm&);
int WeekTest(const tm&, const tm&);
int DayTest(const tm&, const tm&);
int mondif(const tm&, const tm&);
unsigned char isjubel(int);
int tagdif(const tm&, const tm&);
unsigned char snzahl(int);

#endif
