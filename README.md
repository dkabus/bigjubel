# BigJubel Terminal Program

> Calculate jubilees that you might otherwise miss,
> for instance the 7777-day jubilee of your birth!

[BigJubel](https://gitlab.com/dkabus/bigjubel) is fast command line program in `C++` to calculate jubilees.

It is possible to set up a web server or desktop app using:
[`bigjubelweb`](https://gitlab.com/dkabus/bigjubelweb) and
[`bigjubeldesktop`](https://gitlab.com/dkabus/bigjubeldesktop)

The source code of `bigjubel` is open and can be found here:
<https://gitlab.com/dkabus/bigjubel>

For help and usage, check out the manual for BigJubel:

- [`man bigjubel`](https://dkabus.gitlab.io/bigjubel/man.html)

## Installation
On Arch Linux based distibutions, you can install the [AUR package `bigjubel-git`](https://aur.archlinux.org/packages/bigjubel-git/).

Compile with `make` and `gcc`, then install:
```
make
sudo make install
```
