#ifndef INCLUDES_H
#define INCLUDES_H

#include <cstring>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <string>
#include <unistd.h>
#include <vector>

using str = std::string;
using strs = std::vector<std::string>;

#endif
